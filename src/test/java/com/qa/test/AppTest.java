package com.qa.test;

import org.testng.Assert;
import org.testng.annotations.Test;



public class AppTest {
	
	
	@Test
	public void sum() {
		System.out.println("FOr Sum Method");
		int a =10;
		int b=20;
		Assert.assertEquals(30, a+b);
	}
	
	
	@Test
	public void minus() {
		System.out.println("FOr substractor Method");
		int a =10;
		int b=20;
		Assert.assertEquals(10, b-a);
	}

}
